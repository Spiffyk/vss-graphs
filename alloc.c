#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "util.h"

#include "alloc.h"


/* DUMB MALLOC ALLOCATOR **********************************************************************************************/

static int dumb_malloc_allocator_alloc_n(struct vssg_allocator *allocator, size_t n, struct vssg_node **out_nodes)
{
    for (size_t i = 0; i < n; i++) {
        out_nodes[i] = malloc(sizeof(struct vssg_node));
        if (!out_nodes[i])
            return ENOMEM;
    }

    return 0;
}

static void dumb_malloc_allocator_free(struct vssg_allocator *allocator, struct vssg_node *node)
{
    free(node);
}

static struct vssg_allocator dumb_malloc_allocator = {
    .alloc_n = &dumb_malloc_allocator_alloc_n,
    .free = &dumb_malloc_allocator_free
};

struct vssg_allocator *vssg_dumb_malloc_allocator_get()
{
    return &dumb_malloc_allocator;
}


/* CLUSTER MALLOC ALLOCATOR *******************************************************************************************/

struct malloc_cluster; /* Forward decl */

/** A node with a pointer to the beginning of its cluster. */
struct malloc_cluster_entry {
    struct vssg_node node;
    struct malloc_cluster *cluster;
};

/** A cluster of `n` allocated nodes. */
struct malloc_cluster {
    size_t size;
    size_t occupied;
    struct malloc_cluster_entry entries[];
};

static int cluster_malloc_allocator_alloc_n(struct vssg_allocator *allocator, size_t n, struct vssg_node **out_nodes)
{
    struct malloc_cluster *cluster = malloc(sizeof(struct malloc_cluster) + sizeof(struct malloc_cluster_entry[n]));
    if (!cluster)
        return ENOMEM;

    cluster->size = n;
    cluster->occupied = n;
    for (size_t i = 0; i < n; i++) {
        cluster->entries[i].cluster = cluster;
        out_nodes[i] = &cluster->entries[i].node;
    }

    return 0;
}

static void cluster_malloc_allocator_free(struct vssg_allocator *allocator, struct vssg_node *node)
{
    struct malloc_cluster *cluster = ((struct malloc_cluster_entry *) node)->cluster;
    assert(cluster->occupied > 0);
    cluster->occupied -= 1;
    if (cluster->occupied == 0)
        free(cluster);
}

static struct vssg_allocator cluster_malloc_allocator = {
    .alloc_n = cluster_malloc_allocator_alloc_n,
    .free = cluster_malloc_allocator_free
};

struct vssg_allocator *vssg_cluster_malloc_allocator_get()
{
    return &cluster_malloc_allocator;
}


/* LINEAR MMAP ALLOCATOR - COMMON CONSTANTS ***************************************************************************/

#define MMAP_POOL_SIZE 65536


/* LINEAR MMAP ALLOCATOR - NON-FREEABLE *******************************************************************************/

/** A memory pool of graph nodes. */
struct mmap_pool {
    struct mmap_allocator *allocator;        /**< The allocator to which this pool belongs. */
    size_t units;                            /**< Number of allocated nodes. */
    size_t pages;                            /**< Number of committed pages. */
    struct mmap_pool *next;                  /**< Next pool. */
    struct vssg_node nodes[MMAP_POOL_SIZE];  /**< The nodes. */
};

struct mmap_allocator {
    struct vssg_allocator header;
    size_t page_length;
    size_t pool_length;
    struct mmap_pool *tail;
    struct mmap_pool *head;
};

/** Gets a page-aligned length of the `struct mmap_pool`. */
static inline size_t pool_length(size_t page_length)
{
    return (sizeof(struct mmap_pool) / page_length + !!(sizeof(struct mmap_pool) % page_length)) * page_length;
}

static struct mmap_pool *mmap_pool_create(struct mmap_allocator *alloc)
{
    /* Reserve memory space for the new pool */
    struct mmap_pool *pool = mmap(NULL, alloc->pool_length, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pool == MAP_FAILED)
        abort();

    /* Commit the first page in the memory space so that its header can be used */
    int proterr = mprotect(pool, alloc->page_length, PROT_READ | PROT_WRITE);
    if (proterr)
        abort();

    pool->allocator = alloc;
    pool->units = 0;
    pool->pages = 1;
    pool->next = NULL;

    return pool;
}

static void mmap_pool_destroy(struct mmap_pool *pool)
{
    while (pool) {
        struct mmap_pool *next = pool->next;
        int err = munmap(pool, pool->allocator->pool_length);
        if (err)
            abort();
        pool = next;
    }
}

static struct vssg_node *linear_mmap_allocator_alloc(struct mmap_allocator *ma)
{
    if (ma->head->units >= MMAP_POOL_SIZE) {
        /* No space in the last pool, create a new one */
        ma->head->next = mmap_pool_create(ma);
        ma->head = ma->head->next;
    }

    struct mmap_pool *pool = ma->head;
    void *page_boundary;
    while ((void *) &pool->nodes[pool->units + 1] >= (page_boundary = (void *) pool + (pool->pages * ma->page_length))) {
        /* Commit the next page of the memory pool */
        int err = mprotect(page_boundary, ma->page_length, PROT_READ | PROT_WRITE);
        if (err)
            abort();

        pool->pages += 1;
    }

    return &pool->nodes[pool->units++];
}

static int linear_mmap_allocator_alloc_n(struct vssg_allocator *allocator, size_t n, struct vssg_node **out_nodes)
{
    struct mmap_allocator *ma = (struct mmap_allocator *) allocator;
    for (size_t i = 0; i < n; i++) {
        out_nodes[i] = linear_mmap_allocator_alloc(ma);
    }
    return 0;
}

static void linear_mmap_allocator_destroy(struct vssg_allocator *allocator)
{
    struct mmap_allocator *ma = (struct mmap_allocator *) allocator;
    mmap_pool_destroy(ma->tail);
    free(ma);
}

struct vssg_allocator *vssg_linear_mmap_allocator_create()
{
    struct mmap_allocator *allocator = malloc(sizeof(struct mmap_allocator));
    if (!allocator)
        abort();

    size_t page_length = sysconf(_SC_PAGESIZE);
    *allocator = (struct mmap_allocator) {
        .header = {
            .alloc_n = linear_mmap_allocator_alloc_n,
            .free = NULL,
            .destroy_self = linear_mmap_allocator_destroy
        },
        .page_length = page_length,
        .pool_length = pool_length(page_length),
    };

    struct mmap_pool *first_pool = mmap_pool_create(allocator);
    allocator->tail = first_pool;
    allocator->head = first_pool;

    return (struct vssg_allocator *) allocator;
}


/* LINEAR MMAP ALLOCATOR - FREEABLE ***********************************************************************************/

#define MMAP_BITMAP_SIZE (MMAP_POOL_SIZE / 8 + !!(MMAP_POOL_SIZE % 8))

struct mmapf_pool_entry {
    struct vssg_node node;
    struct mmapf_pool *pool;  /**< The pool to which this entry belongs. */
};

/** Pool header. Separated to make `sizeof()` calls easier. */
struct mmapf_pool_header {
    struct mmapf_allocator *allocator;  /**< The allocator to which this pool belongs. */
    size_t id;                          /**< Pool identifier. */
    size_t units;                       /**< Number of allocated nodes. */
    size_t pages;                       /**< Number of committed pages. */
    struct mmapf_pool *prev;            /**< Previous pool. */
    struct mmapf_pool *next;            /**< Next pool. */
    uint8_t bitmap[MMAP_BITMAP_SIZE];   /**< Bitmap of allocated pool elements. */
};

struct mmapf_pool {
    struct mmapf_pool_header header;
    struct mmapf_pool_entry entries[MMAP_POOL_SIZE];
};

struct mmapf_allocator {
    struct vssg_allocator header;
    size_t page_length;
    size_t pool_length;
    size_t next_id;
    struct mmapf_pool *lowest_freed_up;
    struct mmapf_pool *tail;
    struct mmapf_pool *head;
};

/** Gets a page-aligned length that fits `original`. */
static inline size_t page_aligned_size(size_t page_length, size_t original)
{
    return (original / page_length + !!(original % page_length)) * page_length;
}

static struct mmapf_pool *mmapf_pool_create(struct mmapf_allocator *ma)
{
    /* Reserve memory space for the new pool */
    struct mmapf_pool *pool = mmap(NULL, ma->pool_length, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pool == MAP_FAILED) {
        perror("mmap");
        abort();
    }

    /* Commit the first pages in the memory space so that the pool's header can be used */
    int err = mprotect(pool,
                       page_aligned_size(ma->page_length, sizeof(struct mmapf_pool_header)),
                       PROT_READ | PROT_WRITE);
    if (err) {
        perror("initial mprotect");
        abort();
    }

    pool->header.allocator = ma;
    pool->header.id = ma->next_id++;
    pool->header.units = 0;
    pool->header.pages = 1;
    pool->header.next = NULL;

    return pool;
}

static inline void mmapf_pool_destroy(struct mmapf_pool *pool)
{
    int err = munmap(pool, pool->header.allocator->pool_length);
    if (err)
        abort();
}

static void mmapf_pools_list_destroy(struct mmapf_pool *pool)
{
    while (pool) {
        struct mmapf_pool *next = pool->header.next;
        mmapf_pool_destroy(pool);
        pool = next;
    }
}

/** Finds a free index in the specified `bitmap`. */
static ssize_t find_free_index(uint8_t (*bitmap)[MMAP_BITMAP_SIZE])
{
    for (ssize_t byte = 0; byte < MMAP_BITMAP_SIZE; byte++) {
        if ((*bitmap)[byte] == 0xFF)
            continue;

        for (ssize_t bit = 0; bit < 8; bit++) {
            if (!((*bitmap)[byte] & (1 << bit)))
                return (byte * 8) + bit;
        }
    }

    return -1;
}

static struct vssg_node *linear_mmapf_allocator_alloc(struct mmapf_allocator *ma)
{
    struct mmapf_pool *pool = NULL;

    if (ma->lowest_freed_up) {
        pool = ma->lowest_freed_up;
    }

    if (!pool) {
        if (ma->head) {
            /* Let's use the head */
            pool = ma->head;
        } else {
            /* No pools in the allocator - let's create a new one */
            pool = ma->head = ma->tail = mmapf_pool_create(ma);
        }
    }

    while (pool->header.units >= MMAP_POOL_SIZE) {
        if (pool->header.next) {
            /* No space left in the current pool, advance to the next one */
            pool = pool->header.next;
        } else {
            /* No space in the head pool, create a new one */
            assert(pool == ma->head);
            ma->head->header.next = mmapf_pool_create(ma);
            ma->head->header.next->header.prev = ma->head;
            ma->head = ma->head->header.next;
            pool = ma->head;
        }
        ma->lowest_freed_up = pool;
    }

    ssize_t free_index = find_free_index(&pool->header.bitmap);
    assert(free_index >= 0); /* We have already detected that the pool is not full */

    /* Commit pages of the memory pool */
    void *page_boundary = (void *) ((((size_t) &pool->entries[free_index]) / ma->page_length) * ma->page_length);
    size_t prot_size = page_aligned_size(
            ma->page_length,
            (void *) &pool->entries[free_index + 1] - page_boundary);
    int err = mprotect(page_boundary, prot_size, PROT_READ | PROT_WRITE);
    if (err) {
        perror("entry mprotect");
        abort();
    }

    set_bit(pool->header.bitmap, free_index);
    pool->header.units += 1;
    struct mmapf_pool_entry *entry = &pool->entries[free_index];
    entry->pool = pool;
    return &entry->node;
}

static int linear_mmapf_allocator_alloc_n(struct vssg_allocator *allocator, size_t n, struct vssg_node **out_nodes)
{
    struct mmapf_allocator *ma = (struct mmapf_allocator *) allocator;
    for (size_t i = 0; i < n; i++) {
        out_nodes[i] = linear_mmapf_allocator_alloc(ma);
    }
    return 0;
}

static void linear_mmapf_allocator_free(struct vssg_allocator *allocator, struct vssg_node *node)
{
    struct mmapf_allocator *ma = (struct mmapf_allocator *) allocator;
    struct mmapf_pool_entry *entry = (struct mmapf_pool_entry *) node;
    struct mmapf_pool *pool = entry->pool;
    assert(pool->header.allocator == ma);

    /* Mark the node as unused */
    ssize_t entry_index = entry - pool->entries;
    reset_bit(pool->header.bitmap, entry_index);
    pool->header.units -= 1;

    if (pool->header.units == 0) {
        /* If there are no more nodes in this pool, unmap the pool and exit */
        struct mmapf_pool *prev = pool->header.prev;
        struct mmapf_pool *next = pool->header.next;

        if (prev)
            prev->header.next = next;
        else
            ma->tail = next; /* The first pool: set the allocator's tail */

        if (next)
            next->header.prev = prev;
        else
            ma->head = prev; /* The last pool: set the allocator's head */

        if (ma->lowest_freed_up == pool)
            ma->lowest_freed_up = ma->tail;

        mmapf_pool_destroy(pool);
    } else if (!ma->lowest_freed_up || ma->lowest_freed_up->header.id > pool->header.id) {
        /* Otherwise, if the pool has lower ID than the current `lowest_freed_up`, mark it as the `lowest_freed_up` */
        ma->lowest_freed_up = pool;
    }
}

static void linear_mmapf_allocator_destroy(struct vssg_allocator *allocator)
{
    struct mmapf_allocator *ma = (struct mmapf_allocator *) allocator;
    mmapf_pools_list_destroy(ma->tail);
    free(ma);
}

struct vssg_allocator *vssg_linear_mmap_freeable_allocator_create()
{
    struct mmapf_allocator *allocator = malloc(sizeof(struct mmapf_allocator));
    if (!allocator)
        abort();

    size_t page_length = sysconf(_SC_PAGESIZE);
    *allocator = (struct mmapf_allocator) {
        .header = {
            .alloc_n = linear_mmapf_allocator_alloc_n,
            .free = linear_mmapf_allocator_free,
            .destroy_self = linear_mmapf_allocator_destroy
        },
        .page_length = page_length,
        .pool_length = page_aligned_size(page_length, sizeof(struct mmapf_pool)),
    };

    struct mmapf_pool *first_pool = mmapf_pool_create(allocator);
    allocator->tail = first_pool;
    allocator->head = first_pool;

    return (struct vssg_allocator *) allocator;
}
