#ifndef VSSG_ALLOC_H
#define VSSG_ALLOC_H

#include "graph.h"

/** Gets the most basic allocator, using a `malloc` call for each node allocation. The allocator is a 'singleton', so
 * it does not have a `destroy_self` function and does not need to be cleaned up. */
struct vssg_allocator *vssg_dumb_malloc_allocator_get();

/** Gets an allocator that allocates clusters of `n` nodes when `alloc_n` is called. The allocator is a 'singleton', so
 * it does not have a `destroy_self` function and does not need to be cleaned up. */
struct vssg_allocator *vssg_cluster_malloc_allocator_get();

/** Creates a linear allocator that uses `mmap`, `mprotect` and `munmap` to reserve and commit memory pages.
 * This variant of the allocator does not support a `free` operation and gets cleaned up at the end of the program.
 *
 * This allocator has a `destroy_self` function that needs to be called on it in order for it to be properly
 * cleaned up. */
struct vssg_allocator *vssg_linear_mmap_allocator_create();

/** Creates a linear allocator that uses `mmap`, `mprotect` and `munmap` to reserve and commit memory pages.
 * This variant supports `free` and uses bitmaps to mark pool positions as (un-)allocated.
 *
 * This allocator has a `destroy_self` function that needs to be called on it in order for it to be properly
 * cleaned up. */
struct vssg_allocator *vssg_linear_mmap_freeable_allocator_create();

#endif /* VSSG_ALLOC_H */
