/**@file Graph allocation tester. */

#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/resource.h>

#include "alloc.h"
#include "graph.h"
#include "grinst.h"
#include "util.h"

static long get_total_pflts()
{
    struct rusage r;
    getrusage(RUSAGE_SELF, &r);

    return r.ru_minflt + r.ru_majflt;
}

/** Gets the total number of page faults since the last call to this function. */
static long get_pflts()
{
    static long last = 0;
    long total = get_total_pflts();
    long result = total - last;
    last = total;
    return result;
}

static struct vssg_allocator *get_allocator(const char *name)
{
    if (strcmp(name, "dumb-malloc") == 0)
        return vssg_dumb_malloc_allocator_get();
    if (strcmp(name, "cluster-malloc") == 0)
        return vssg_cluster_malloc_allocator_get();
    if (strcmp(name, "linear-mmap") == 0)
        return vssg_linear_mmap_allocator_create();
    if (strcmp(name, "linear-mmap-freeable") == 0)
        return vssg_linear_mmap_freeable_allocator_create();

    return NULL;
}

static void reset_marks(struct vssg_graph *g)
{
    uint64_t pre_reset = clock_millis(CLOCK_MONOTONIC);
    for (struct vssg_graph_it it = vssg_graph_it_start(g); !vssg_graph_it_end(&it); vssg_graph_it_next(&it)) {
        vssg_graph_it_get(&it)->mark = false;
    }
    uint64_t reset_time = clock_millis(CLOCK_MONOTONIC) - pre_reset;
    printf("Graph marks reset in %" PRIu64 " ms (%ld page-faults)\n", reset_time, get_pflts());
}

static void breadth_first_sum(struct vssg_graph *g)
{
    uint64_t pre_sum = clock_millis(CLOCK_MONOTONIC);
    struct vssg_queue q;
    vssg_queue_init(&q);
    vssg_queue_push(&q, vssg_graph_get_any(g));

    int64_t sum = 0;

    while (q.length > 0) {
        struct vssg_node *node = vssg_queue_pop(&q);
        sum += node->value;

        do {
            for (size_t i = 0; i < VSSG_NODE_MAX_REFS; i++) {
                struct vssg_node *candidate = node->refs[i];
                if (candidate && !candidate->mark) {
                    candidate->mark = true;
                    vssg_queue_push(&q, candidate);
                }
            }
            node = node->ext;
        } while (node);
    }

    vssg_queue_destroy(&q);
    uint64_t sum_time = clock_millis(CLOCK_MONOTONIC) - pre_sum;

    printf("Graph breadth-first-summed to %" PRIi64 " in %" PRIu64 " ms (%ld page-faults)\n", sum, sum_time, get_pflts());
}

static void depth_first_sum(struct vssg_graph *g)
{
    int64_t pre_sum = clock_millis(CLOCK_MONOTONIC);
    struct vssg_stack s;
    vssg_stack_init(&s);
    vssg_stack_push(&s, vssg_graph_get_any(g));

    int64_t sum = 0;

    while (s.length > 0) {
        struct vssg_node *node = vssg_stack_pop(&s);
        sum += node->value;

        do {
            for (size_t i = 0; i < VSSG_NODE_MAX_REFS; i++) {
                struct vssg_node *candidate = node->refs[i];
                if (candidate && !candidate->mark) {
                    candidate->mark = true;
                    vssg_stack_push(&s, candidate);
                }
            }
            node = node->ext;
        } while (node);
    }

    vssg_stack_destroy(&s);
    uint64_t sum_time = clock_millis(CLOCK_MONOTONIC) - pre_sum;

    printf("Graph depth-first-summed to %" PRIi64 " in %" PRIu64 " ms (%ld page-faults)\n", sum, sum_time, get_pflts());
}

int main(int argc, char **argv)
{
    if (argc <= 1) {
        fprintf(stderr, "Usage: %s <input_file> [allocator_name]\n", argv[0]);
        return 1;
    }

    struct vssg_allocator *allocator;
    if (argc >= 3) {
        allocator = get_allocator(argv[2]);
        if (!allocator) {
            fprintf(stderr, "Unknown allocator '%s'. Supported allocators:\n"
                    "dumb-malloc, cluster-malloc, linear-mmap, linear-mmap-freeable", argv[2]);
            return 1;
        }
    } else {
        allocator = vssg_dumb_malloc_allocator_get();
    }

    int retval = 0;
    FILE *in = fopen(argv[1], "r");
    if (!in) {
        perror("Could not open grinst file");
        retval = 1;
        goto exit_alloc_destroy;
    }

    struct vssg_graph g;
    vssg_graph_init(&g, allocator);

    uint64_t pre_read = clock_millis(CLOCK_MONOTONIC);
    int err = vssg_inst_interpret_file(&g, in);
    if (err) {
        fprintf(stderr, "Error in GRINST file: %s\n", strerror(err));
        retval = err;
        goto exit_close;
    }
    uint64_t read_time = clock_millis(CLOCK_MONOTONIC) - pre_read;
    printf("Graph (%zu nodes) rebuilt in %" PRIu64 " ms (%ld page-faults)\n", g.graph_size, read_time, get_pflts());

    reset_marks(&g);
    breadth_first_sum(&g);
    reset_marks(&g);
    depth_first_sum(&g);

    vssg_graph_destroy(&g);

    printf("Done. Total page-faults: %ld\n", get_total_pflts());

exit_close:;
    int in_end = fclose(in);
    if (in_end == EOF) {
        perror("Grinst file not closed properly");
        retval = 1;
    }
exit_alloc_destroy:
    vssg_alloc_destroy(allocator);
    return retval;
}
