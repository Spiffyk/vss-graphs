\section{Úvod}

Existuje velká škála faktorů, které mají vliv na výkonnost počítačového programu. Vývoj výkonného programu začíná volbou správného algoritmu, který pro daný případ užití bude mít co nejmenší výpočetní složitost. Pokračujeme volbou vhodného jazyka, který programátorovi umožní psát výkonný kód -- zde rovněž záleží na optimalizačních schopnostech překladače. Velmi také záleží na výkonnosti cílového stroje -- architektura a frekvence procesoru, velikost a rychlost pamětí apod.

Často opomíjeným faktem však je to, že nezanedbatelný vliv na výkon programu může mít i fyzické rozmístění dat v paměti. Procesory totiž obsahují hierarchii cache pamětí, jimiž urychlují práci s daty, neboť přímá práce s RAM vyžaduje delší režii. Teoreticky, čím více v daném časovém úseku může procesor pracovat s cache, aniž by do ní musel kopírovat další data z hlavní paměti, tím výkonnější program bude.

Cílem této semestrální práce je implementovat program v jazyce C, který bude podporovat několik technik alokace paměti a umožní otestovat, jaký vliv jednotlivé techniky na výkon programu skutečně mají. Využije se k tomu náhodně generovaný graf, který bude procházen grafovými algoritmy. Sledujeme dobu alokace grafu, dobu průchodu a využití cache paměti.

\newpage

\section{Reprezentace grafu}

V implementovaném programu je graf reprezentován několika strukturami. Nejdůležitější strukturou je \texttt{vssg\_node} představující uzel v grafu. Uzel obsahuje identifikátor (sekvenční v rámci grafu), hodnotu a dva typy pointerů na další uzly: \texttt{refs} na uzly, směrem \emph{do nichž} z tohoto uzlu vede hrana a \texttt{backrefs} na uzly, \emph{z nichž} vede hrana do tohoto uzlu.

Druhou strukturou je \texttt{vssg\_graph} -- ta představuje referenci na celý graf a obsahuje hešovací tabulku uzlů. Klíčem v této hešovací tabulce je identifikátor uzlu.


\section{Generátor grafů a interpreter}

Pro zajištění opakovatelnosti experimentů byl pro tuto práci vytvořen interpreter, který podle instrukcí jednoduchého \emph{bajtkódu} sekvenčně sestaví graf. Pro generování tohoto bajtkódu byl poté vytvořen generátor grafů, který vytváří soubory s instrukcemi.

\subsection{Bajtkód}

Bajtkód obsahuje celkem 5 operací:

\begin{itemize}
    \item \texttt{NOP} -- nečinná instrukce (pro úplnost; nepoužito),
    \item \texttt{CREATE} -- alokuje a přidá do grafu $n$ uzlů,
    \item \texttt{DELETE} -- odstraní z grafu uzel s daným ID,
    \item \texttt{SET\_VALUE} -- nastaví danou hodnotu uzlu s daným ID a
    \item \texttt{CONNECT} -- vytvoří hranu z jednoho uzlu do druhého (podle ID).
\end{itemize}

Každá instrukce bajtkódu má délku 24 bajtů. Začíná 32bitovým kódem operace (\emph{opcode}), následuje 32 bitů rezervy a poté 128 bitů parametru -- význam parametru je dán konkrétní operací. Pořadí bajtů v číselných hodnotách v instrukcích je \emph{little endian}.

Kompletní knihovna funkcí a struktur pro práci s bajtkódem se nachází ve zdrojových souborech \texttt{grinst.h} a \texttt{grinst.c}.

\subsection{Generátor grafů}

Pro generování instrukcí byl implementován program \texttt{graphgen}. Ten pseudonáhodně generuje graf na základě uživatelem zadané posloupnosti celých čísel -- kladná čísla značí vytváření nových uzlů, záporná značí jejich odstraňování. Uzly jsou vytvářeny v menších skupinách, v nichž tvoří hustě propojené podgrafy. Jednotlivé podgrafy jsou poté několika hranami propojeny se zbytkem hlavního grafu. Velikosti skupin a počty hran jsou pseudonáhodně generovány.

V generátoru je použita vlastní implementace lineárního kongruentního generátoru pseudonáhodných čísel (LCG). To z toho důvodu, že jeho varianta vestavěná v knihovně GCC má relativně malý rozsah generovaných čísel. Byly proto použity konstanty LCG z knihovny \emph{musl}. Implementace se nachází v souboru \texttt{util.h}.


\section{Alokátory a testovací program}

K porovnávání jednotlivých metod alokací byl implementován program \texttt{alloctest}. Ten využívá itepreter popisovaný v předchozí sekci k rekonstrukci grafu za použití daného alokátoru. Po dokončení rekonstrukce grafu nad ním provede dvě sečtení hodnot jeho uzlů -- nejprve \emph{breadth-first} a poté \emph{depth-first}. Před každým sčítáním resetuje příznaky, které označují již navštívené uzly.

Jednotlivé alokátory splňují společné rozhraní, které je dané pointery na funkce uvnitř struktury \texttt{vssg\_allocator}. Tyto funkce jsou tři: \texttt{alloc\_n} pro alokaci $n$ uzlů, \texttt{free} pro uvolnění daného uzlu a \texttt{destroy\_self} pro vyčištění alokátoru a uvolnění jeho prostředků. Pouze \texttt{alloc\_n} je povinnou funkcí, ostatní nic nedělají, pokud je hodnota daného pointeru \texttt{NULL}.

Program implementuje celkem čtyři alokátory popsané v následujících podsekcích.

\subsection{Jednoduchý malloc}

Nejprimitivnější alokátor v této práci. Provádí volání standardní funkce \texttt{malloc} pro každý uzel zvlášť. Při odstraňování uzlů volá pro každý standardní funkci \texttt{free}.

\subsection{Skupinový malloc}

Jelikož jsou uzly generovány po skupinách, lze provést optimalizaci v podobě hromadné alokace. Místo, aby se funkce \texttt{malloc} volala pro alokaci každého uzlu, zavolá se pro celý \emph{cluster}. Ten obsahuje dva čítače -- celkový počet uzlů a počet aktivních uzlů. Po těchto čítačích následuje pole samotných uzlů.

Elementy v clusteru obsahují kromě samotných dat uzlu navíc pointer na začátek clusteru. Ten je použit při volání uvolňovací procedury k nalezení čítače aktivních uzlů a jeho dekrementaci.

Jakmile čítač aktivních uzlů dosáhne hodnoty $0$, je celý cluster uvolněn pomocí funkce \texttt{free}.

\subsection{Lineární mmap}

Dále byl v práci implementován vlastní lineární alokátor. Ten používá funkci \texttt{mmap} k rezervaci paměťových \emph{poolů}. Při této rezervaci jsou alokovány stránky virtuální paměti, operační systém jim však v tu chvíli okamžitě nemusí přiřadit stránky fyzické a může dosud nepotvrzenou fyzickou paměť dále využívat pro jiné účely.

Při vytváření uzlů alokátor kontroluje hranice stránek a při jejich překročení volá funkci \texttt{mprotect}, čímž si od operačního systému vyžádá potvrzení stránky.

Pool obsahuje čítač uzlů, čítač stránek, pole uzlů a pointer na další pool. Ve chvíli, kdy v poolu již není místo, je vytvořen nový a pointer na něj je zapsán do poolu předchozího, čímž pooly tvoří spojový seznam.

Tato varianta alokátoru neumožňuje uvolňování jednotlivých uzlů. Uzly, které již byly z grafu odstraněny, tak stále zabírají paměť, dokud celý program neskončí a neuvolní se celý alokátor.

\subsection{Lineární mmap s uvolňováním}

Toto je varianta předchozího alokátoru s tím rozdílem, že tento umožňuje průběžné uvolňování. Kromě čítačů uzlů pooly tohoto alokátoru obsahují také bitmapy. Ty značí, které prvky poolu jsou alokované. Tímto způsobem lze znovu použít uvolněné uzly.

Podobně jako v případě \emph{skupinového mallocu} zde každý uzel obsahuje pointer na začátek poolu, aby byl alokátor schopen určit, se kterým poolem pracuje pouze pomocí předaného pointeru na uzel.


\section{Testy a jejich výsledky}

Implementovaný program byl testován na počítači s procesorem \emph{AMD Ryzen 7 5800X} a $32$~GiB RAM. Použitým operačním systémem byl \emph{Manjaro Linux} s verzí kernelu \texttt{5.15.15-1-MANJARO}.

Program byl testován pomocí přiložených shell skriptů \texttt{test\_perf.sh} a \texttt{test\_massif.sh}.

Skript \texttt{test\_perf.sh} spouští program nad stejným souborem pro každý typ implementovaného alokátoru a vypisuje informace o přístupech do cache pomocí programu \texttt{perf stat}. Tímto skriptem byl program spuštěn pětkrát pro potlačení zdánlivého nedeterminismu časové náročnosti programu kvůli režii OS. Výsledky byly zaznamenány do přiložené tabulky. Průměrné vysledky lze vidět v tabulce \ref{tab:alloctest}.

Skript \texttt{test\_massif.sh} opět spouští program nad stejným souborem pro každý typ alokátoru a pomocí nástroje \emph{Valgrind Massif} zaznamenává průběh využití stránek paměti. Maxima využití paměti byla zaznamenána do dokumentace. Výsledné \texttt{massif.out} soubory jsou přiloženy.

\newpage

\begin{table}[h!]
    \centering
    \begin{tabular}{| c | r | r | r | r | r | r | r |}
        \hline
        ~ & \textbf{Alokace} & \textbf{MR} & \textbf{BFS} & \textbf{DFS} & \textbf{Celkem} & \textbf{L1 a.} & \textbf{L1 m.} \\
        ~ & [ms]           & [ms]        & [ms]         & [ms]         & [ms]           & [mld.]         & [\%]           \\
        \hline
        \textbf{D} & $25~183.20$ & $14.80$ & $305.00$ & $268.80$ & $25~847.80$ & $18.927$ & $60.70$ \\
        \textbf{C} & $28~949.40$ & $17.40$ & $298.00$ & $263.60$ & $29~592.40$ & $18.576$ & $56.78$ \\
        \textbf{L} & $28~312.20$ & $16.40$ & $301.80$ & $268.80$ & $30~950.80$ & $18.565$ & $56.84$ \\
        \textbf{F} & $32~495.80$ & $15.40$ & $303.00$ & $265.80$ & $33~139.60$ & $25.431$ & $42.23$ \\
        \hline
    \end{tabular}
    \caption{Průměrné výsledky testování jednotlivých alokátorů}
    \label{tab:alloctest}
\end{table}

V tabulce \ref{tab:alloctest} lze vidět průměrné výsledky testování pro jednotlivé alokátory -- \textbf{D}:~jednoduchý malloc, \textbf{C}:~skupinový malloc, \textbf{L}:~lineární mmap, \textbf{F}:~lineární mmap s uvolňováním.

Tabulka porovnává časy rekonstrukce grafu (\textbf{Alokace}), resetu příznaků (\textbf{MR}), prohledávání (\textbf{BFS} a \textbf{DFS}) a celkové časy běhu (\textbf{Celkem}). Také porovnává celkové počty přístupů do L1 cache (\textbf{L1 a.}) a procentuální zastoupení cache misses (\textbf{L1 m.}).

Podrobné výsledky jednotlivých běhů programu lze vidět v přiloženém souboru \texttt{alloctest.ods} ve formátu OpenDocument.

Maxima využití paměti zaznamenané nástrojem Massif jsou následující:

\begin{itemize}
    \item \textbf{Jednoduchý malloc:} $691.9$ MiB
    \item \textbf{Skupinový malloc:} $723.5$ MiB
    \item \textbf{Lineární mmap:} $719.4$ MiB
    \item \textbf{Lineární mmap s uvolňováním:} $705.6$ MiB
\end{itemize}

Z výsledných dat lze usoudit, že při technice \emph{jednoduchého mallocu} je celkový běh programu nejrychlejší především z toho důvodu, že je v něm nejrychlejší alokace. Rovněž je při jeho použití nejmenší využití paměti. Průchod grafem je však ze všech testovaných metod nejpomalejší, a to jak v případě \emph{breadth-first}, tak v případě \emph{depth-first} prohledávání. V případě \emph{depth-first} se však průměr naměřených časů schoduje s \emph{lineárním mmapem}. Nejrychlejší byl tento způsob v resetování příznaků uzlů.

Při použití techniky \emph{skupinového mallocu} program dosahoval nejlepších časových výsledků při prohledávání. To se dá připsat faktu, že uzly, mezi kterými je největší množství vazeb, jsou v paměti fyzicky umístěny nejvíce pohromadě. Režie alokátoru, zejména pak pointery na začátky clusterů, které jsou umístěny za každým uzlem, však způsobuje největší zatížení paměti ze všech testovaných metod. Jistou optimalizací využití paměti by mohlo být řešení, které umisťuje pointery na začátek clusteru na začátek každé stránky clusteru -- tak by bylo možné pro každý alokovaný uzel automatizovaně nalézt začátek clusteru, kterému náleží, ale nebylo by nutné alokovat 8 bajtů navíc pro každý uzel. Toto řešení však nebylo implementováno.

\emph{Lineární mmap} dosahuje rozumných výsledků při \emph{depth-first} prohledávání grafu, avšak jeho celkové časové výsledky byly spíše zklamáním, a to zejména oproti \emph{skupinovému mallocu}. V našem testovacím případě neměl takovou paměťovou náročnost, jako \emph{skupinový malloc}, avšak při delším běhu s rozsáhlejším přidáváním a odstraňováním uzlů by jeho náročnost byla jistě větší, neboť nepodporuje žádnou formu uvolňování paměti.

Varianta \emph{lineárního mmapu s uvolňováním} přidává možnost uvolňování paměti a díky tomu je na druhé příčce v paměťové náročnosti ze všech testovaných metod. Má však daleko největší režii na samotnou alokaci. Jeho paměťová náročnost by mohla být dále optimalizována podobně jako v případě \emph{skupinového mallocu}.

Z dat lze vidět, že k nejhoršímu využití cache dochází při alokaci \emph{jednoduchým mallocem}. Zejména u poslední metody alokace však nejsou výsledky využití L1 cache příliš informativní, neboť zde kvůli operacím s bitmapou dochází k mnohem četnějšímu přistupování do cache při alokaci kvůli operacím nad bitmapou. Při samotném prohledávání však nelze očekávat velký rozdíl oproti ostatním metodám. Program \emph{Perf Stat} neumožňuje zkoumat využití cache v čase, tudíž tyto výsledky bohužel nejsou příliš průkazné.


\section{Uživatelská příručka}

Semestrální práce se sestává ze dvou programů pro příkazový řádek -- \texttt{graphgen} a \texttt{alloctest}. Programy jsou napsány v jazyce C pro operační systém GNU/Linux.

\subsection{Sestavení}

Projektové soubory jsou vytvořeny pro program \emph{CMake}. K sestavení programu se všemi optimalizacemi překladače lze použít následující příkazy v adresáři se zdrojovými texty:

\begin{verbatim}
$ mkdir build-release && cd build-release
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ cmake --build .
\end{verbatim}

Tímto v adresáři \texttt{build-release} vzniknou data programu CMake a soubory \texttt{graphgen} a \texttt{alloctest}, které obsahují spustitelné programy.

\subsection{Spuštění}

\subsubsection{graphgen}

Program \texttt{graphgen} slouží ke generování pseudonáhodných grafů. Jako první povinný argument přijímá název výstupního binárního souboru, do nejž bude zapsán bajtkód s instrukcemi pro sestavení grafu. Jako další argumenty přijímá počty uzlů -- kladné pro přidávání uzlů do grafu, záporné pro jejich odebírání. Alespoň jeden počet uzlů musí být zadán a musí být kladný.

Příklad spuštění programu:

\begin{verbatim}
$ ./graphgen ../data/graph.grinst 500000 -8000 10000 -100 200
\end{verbatim}

Takto program vytvoří graf, do nejž nejprve přidá $500~000$ uzlů, poté $8~000$ uzlů odebere, poté přidá $10~000$ uzlů, odebere $100$ a nakonec přidá $200$ uzlů. Uzly přidává po menších skupinách, aby vzniklo množství vzájemně propojených podgrafů. Celý proces generování je nahrán a uložen do souboru \texttt{../data/graph.grinst} jako bajtkód s instrukcemi.

\subsubsection{alloctest}

Program \texttt{alloctest} interpretuje instrukce ze vstupního souboru, čímž pomocí vybraného alokátoru zrekonstruuje graf. Rekonstruovaný graf poté projde \emph{breadth-first} a \emph{depth-first} algoritmy.

Prvním argumentem pro program \texttt{alloctest} je cesta k souboru s bajtkódem, druhým je název alokátoru:

\begin{itemize}
    \item \texttt{dumb-malloc} -- jednoduchý malloc,
    \item \texttt{cluster-malloc} -- skupinový malloc,
    \item \texttt{linear-mmap} -- lineární mmap,
    \item \texttt{linear-mmap-freeable} -- lineární mmap s uvolňováním
\end{itemize}

Příklad spuštění programu:

\begin{verbatim}
$ ./alloctest ../data/graph.grinst linear-mmap
\end{verbatim}

Takto je program spuštěn nad souborem \texttt{../data/graph.grinst} a k alokaci použije \emph{lineární mmap}.

\newpage

\section{Závěr}

Implementovaný program demonstruje, že různé techniky alokace mají vliv na jeho výkonnost. Ukazuje se, že v případě delšího opakovaného používání stejných alokovaných dat se z hlediska rychlosti více vyplatí data alokovat po větších uskupeních, aby se data fyzicky nacházela v paměti poblíž sebe. Zároveň je zde však vidět, že funkce \texttt{malloc} v operačním systému GNU/Linux velmi rychle alokuje drobné celky dat, což může naopak zlepšit výkon u programů, které jednou vytvoří svá data, a jednorázově je zpracují.

Výsledky ukazují, že pro různé typy programů lze použít různé techniky alokace paměti a zřejmě neexistuje jeden univerzální správný. Práce zároveň naznačuje, že touto problematikou se lze zabývat ještě hlouběji a bylo by možné provést další množství experimentů, které by mohly odhalit další zajímavé výsledky.