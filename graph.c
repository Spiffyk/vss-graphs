#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

#include "graph.h"


/* NODE ***************************************************************************************************************/

/** A typedef for an ugly C construct, used in `node_add_ref` below for some pointer offset magic. */
typedef struct vssg_node *(vssg_refs_t)[VSSG_NODE_MAX_REFS];

/** Node reference finder function. Finds a reference to `ref` in `node->##offset`, or an empty place in it
 * to add `ref` to.
 * If `allocator` is not `NULL`, `node` may be 'extended' to accommodate for a newly added reference.
 *
 * Return value may be:
 *  - pointer to a filled `struct vssg_node *` -- `ref` exists
 *  - pointer to a `NULL` `struct vssg_node *` -- `ref` does not exist and can be added
 *  - `NULL`                                   -- `ref` does not exist and there is no place for it */
static struct vssg_node **node_find_ref(struct vssg_node *node, vssg_refs_t *offset, struct vssg_node *ref,
                                        struct vssg_allocator *allocator)
{
    /* Yea, I normally don't like putting a buttload of comments in functions, but I think this one is complicated
     * and not-obvious, so in my head this step-by-step annotation is justified. */

    struct vssg_node **first_empty_result = NULL;
tailrec:;
    vssg_refs_t *refs = offsetattr(node, offset);

    /* Base run-through: find `ref` and last empty result */
    for (size_t i = 0; i < VSSG_NODE_MAX_REFS; i++) {
        if ((*refs)[i]) {
            if ((*refs)[i] == ref) {
                /* `ref` is already in this node - return it */
                return &(*refs)[i];
            }
        } else if (!first_empty_result) {
            /* This is the first empty place in the refs - take note */
            first_empty_result = &(*refs)[i];
        }
    }

    /* No space in the current node - use extension */

    if (node->ext) {
        /* Extension node exists - switch to it and repeat the whole function */
        node = node->ext;
        goto tailrec;
    } else {
        /* Extension node does not exist - one of two: */
        if (allocator && !first_empty_result) {
            /* Allocator provided: allocate new extension and return the first ref */
            int err = vssg_alloc_n(allocator, 1, &node->ext);
            if (err || !node->ext)
                abort(); /* ENOMEM */

            /* Initialize extension node */
            *node->ext = (struct vssg_node) {
                .id = node->id,
                .value = node->value,
                .ext_backref = node
            };

            /* The extension's refs (by offset) */
            vssg_refs_t *extrefs = offsetattr(node->ext, offset);
            return *extrefs;
        } else {
            /* Allocator NOT provided: return first empty place for a ref (if any, may just return NULL) */
            return first_empty_result;
        }
    }
}

static void node_add_ref(struct vssg_node *node, vssg_refs_t *offset, struct vssg_node *ref,
                         struct vssg_allocator *allocator)
{
    struct vssg_node **place = node_find_ref(node, offset, ref, allocator);
    assert(place);
    if (!*place) /* `ref` not found in `node` and an empty place is available - add `ref` */
        *place = ref;
}

static bool node_remove_ref(struct vssg_node *node, vssg_refs_t *offset, struct vssg_node *ref)
{
    struct vssg_node **found = node_find_ref(node, offset, ref, NULL);
    if (!found || !*found)
        return false; /* `ref` not found in `node`, so not removed */
    else {
        *found = NULL;
        return true;
    }
}

void vssg_node_connect(struct vssg_node *src, struct vssg_node *dst, struct vssg_allocator *allocator)
{
    assert(allocator);
    node_add_ref(src, offsetof(struct vssg_node, refs), dst, allocator);
    node_add_ref(dst, offsetof(struct vssg_node, backrefs), src, allocator);
}

void vssg_node_disconnect(struct vssg_node *src, struct vssg_node *dst)
{
    node_remove_ref(src, offsetof(struct vssg_node, refs), dst);
    node_remove_ref(dst, offsetof(struct vssg_node, backrefs), src);
}

void vssg_node_disconnect_all(struct vssg_node *dst)
{
    struct vssg_node *node = dst;
    do {
        for (size_t i = 0; i < VSSG_NODE_MAX_REFS; i++) {
            if (node->refs[i])
                node_remove_ref(node->refs[i], offsetof(struct vssg_node, backrefs), dst);
            if (node->backrefs[i])
                node_remove_ref(node->backrefs[i], offsetof(struct vssg_node, refs), dst);
        }

        node = node->ext; /* Switch to extension node (if any exists) */
    } while (node);
}


/* HASHTABLE **********************************************************************************************************/

/** Destroys the specified hashtable and all of its linked extension hashtables. */
static void vssg_hashtable_destroy(struct vssg_hashtable *t)
{
    if (!t)
        return;
    vssg_hashtable_destroy(t->next);
    free(t);
}


/* GRAPH **************************************************************************************************************/

/** Frees the specified node and all of its extension nodes. */
static void vssg_graph_free_node(struct vssg_allocator *allocator, struct vssg_node *node)
{
    while (node->ext)
        node = node->ext;

    do {
        struct vssg_node *back = node->ext_backref;
        vssg_alloc_free(allocator, node);
        node = back;
    } while (node);
}

void vssg_graph_destroy(struct vssg_graph *graph)
{
    for (struct vssg_graph_it it = vssg_graph_it_start(graph); !vssg_graph_it_end(&it); vssg_graph_it_next(&it)) {
        vssg_graph_free_node(graph->allocator, vssg_graph_it_get(&it));
    }
    vssg_hashtable_destroy(graph->hashtable.next);
}

static inline size_t vssg_id_hash(uint64_t node_id)
{
    /* IDs are sequential, which will lead to the least collisions anyway, so hash function is identity for now.
     * And it's inline, so it's free, but easy to change later if needed. */
    return (size_t) node_id;
}

static void vssg_graph_insert(struct vssg_graph *graph, struct vssg_node *node)
{
    struct vssg_hashtable *t = &graph->hashtable;
    const size_t hash = vssg_id_hash(node->id);

search_table:;
    struct vssg_node **nodes = t->table[hash % VSSG_HASHTABLE_SIZE];
    for (size_t i = 0; i < VSSG_HASHTABLE_COLSIZE; i++) {
        if (!nodes[i]) {
            nodes[i] = node;
            graph->graph_size += 1;
            return;
        }
        assert(nodes[i]->id != node->id);
    }

    if (!t->next) {
        t->next = calloc(1, sizeof(struct vssg_hashtable));
        if (!t->next)
            abort();
    }

    t = t->next;
    goto search_table;
}

int vssg_graph_create_nodes(struct vssg_graph *graph, size_t num_nodes, struct vssg_node **out_nodes)
{
    int err;

    if (!out_nodes) /* No `out_nodes` provided - create our own */
        out_nodes = alloca(num_nodes * sizeof(struct vssg_node *));

    err = vssg_alloc_n(graph->allocator, num_nodes, out_nodes);
    if (err)
        return err;

    for (size_t i = 0; i < num_nodes; i++) {
        struct vssg_node *node = out_nodes[i];
        *node = (struct vssg_node) {
            .id = graph->next_id++
        };
        vssg_graph_insert(graph, node);
    }

    return 0;
}

struct vssg_node **vssg_graph_find(struct vssg_graph *graph, uint64_t node_id)
{
    struct vssg_hashtable *t = &graph->hashtable;
    const size_t hash = vssg_id_hash(node_id);

search_table:;
    struct vssg_node **nodes = t->table[hash % VSSG_HASHTABLE_SIZE];
    for (size_t i = 0; i < VSSG_HASHTABLE_COLSIZE; i++) {
        if (nodes[i] && nodes[i]->id == node_id)
            return &nodes[i];
    }

    if (t->next) {
        t = t->next;
        goto search_table;
    }

    return NULL;
}

bool vssg_graph_remove_node(struct vssg_graph *graph, uint64_t node_id)
{
    struct vssg_node **ht_node = vssg_graph_find(graph, node_id);
    if (!ht_node)
        return false;

    vssg_node_disconnect_all(*ht_node);
    vssg_graph_free_node(graph->allocator, *ht_node);
    *ht_node = NULL;
    return true;
}

struct vssg_node *vssg_graph_get_any(struct vssg_graph *graph)
{
    if (graph->graph_size == 0)
        return NULL;

    struct vssg_hashtable *t = &graph->hashtable;

search_table:;
    for (size_t i = 0; i < VSSG_HASHTABLE_SIZE; i++) {
        for (size_t j = 0; j < VSSG_HASHTABLE_COLSIZE; j++) {
            if (t->table[i][j])
                return t->table[i][j];
        }
    }

    if (t->next) {
        t = t->next;
        goto search_table;
    }

    return NULL;
}


/* GRAPH ITERATOR *****************************************************************************************************/

struct vssg_graph_it vssg_graph_it_start(struct vssg_graph *graph)
{
    struct vssg_graph_it it = {
        .ht = &graph->hashtable
    };

    if (!it.ht->table[0][0])
        vssg_graph_it_next(&it);

    return it;
}

void vssg_graph_it_next(struct vssg_graph_it *it)
{
    if (!it->ht)
        return;

    do {
        it->col++;
        if (it->col >= VSSG_HASHTABLE_COLSIZE) {
            it->col = 0;
            it->i++;

            if (it->i >= VSSG_HASHTABLE_SIZE) {
                it->i = 0;
                it->ht = it->ht->next;
            }
        }
    } while (it->ht && !it->ht->table[it->i][it->col]);
}


/* NODE QUEUE *********************************************************************************************************/

void vssg_queue_init(struct vssg_queue *q)
{
    q->start = 0;
    q->length = 0;
    q->capacity = VSSG_QUEUE_BASE_LENGTH;
    q->buffer = malloc(sizeof(struct vssg_node *[q->capacity]));
    if (!q->buffer)
        abort();
}

void vssg_queue_destroy(struct vssg_queue *q)
{
    free(q->buffer);
}

void vssg_queue_push(struct vssg_queue *q, struct vssg_node *node)
{
    assert(node);

    if (q->length == q->capacity) {
        size_t new_capacity = q->capacity * 2;
        struct vssg_node **new_buffer = malloc(sizeof(struct vssg_node *[new_capacity]));
        size_t c1_length = q->capacity - q->start;
        memcpy(new_buffer, &q->buffer[q->start], sizeof(struct vssg_node *[c1_length]));
        size_t c2_length = q->start;
        if (c2_length > 0)
            memcpy(&new_buffer[c1_length], q->buffer, sizeof(struct vssg_node *[c2_length]));

        free(q->buffer);

        q->start = 0;
        q->capacity = new_capacity;
        q->buffer = new_buffer;
    }

    q->buffer[(q->start + q->length) % q->capacity] = node;
    q->length += 1;
}

struct vssg_node *vssg_queue_pop(struct vssg_queue *q)
{
    if (q->length == 0)
        return NULL;

    struct vssg_node *result = q->buffer[q->start];
    q->start = (q->start + 1) % q->capacity;
    q->length -= 1;
    return result;
}


/* NODE STACK *********************************************************************************************************/

void vssg_stack_init(struct vssg_stack *s)
{
    s->length = 0;
    s->capacity = VSSG_STACK_BASE_LENGTH;
    s->buffer = malloc(sizeof(struct vssg_node *[s->capacity]));
    if (!s->buffer)
        abort();
}

void vssg_stack_destroy(struct vssg_stack *s)
{
    free(s->buffer);
}

void vssg_stack_push(struct vssg_stack *s, struct vssg_node *node)
{
    if (s->length == s->capacity) {
        s->capacity *= 2;
        s->buffer = realloc(s->buffer, sizeof(struct vssg_node *[s->capacity]));
    }

    s->buffer[s->length] = node;
    s->length += 1;
}

struct vssg_node *vssg_stack_pop(struct vssg_stack *s)
{
    s->length -= 1;
    return s->buffer[s->length];
}
