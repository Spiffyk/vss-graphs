#ifndef VSSG_GRAPH_H
#define VSSG_GRAPH_H

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>

/* Forward declarations */
struct vssg_allocator;


/* NODE ***************************************************************************************************************/

#define VSSG_NODE_MAX_REFS 24   /**< The length of ref arrays in nodes. */

/** A graph node. */
struct vssg_node {
    uint64_t id;                                      /**< Unique identifier. */
    int64_t value;                                    /**< Random value. */
    bool mark;
    struct vssg_node *refs[VSSG_NODE_MAX_REFS];       /**< Outgoing references to neighbour nodes. NULL-terminated. */
    struct vssg_node *backrefs[VSSG_NODE_MAX_REFS];   /**< Backward references to nodes. NULL-terminated. */
    struct vssg_node *ext;                            /**< Pointer to extension node.
                                                       * May be used when the number of neighbours or backrefs is
                                                       * larger than `VSSG_NODE_MAX_NEIGHBORS`. */
    struct vssg_node *ext_backref;                    /**< Pointer back to the extended node. */
};

/** Creates a line from node `src` to node `dst`, connecting them directionally. Uses `allocator` to allocate extension
 * nodes if needed. */
void vssg_node_connect(struct vssg_node *src, struct vssg_node *dst, struct vssg_allocator *allocator);

/** Removes the line from node `src` to node `dst`, disconnecting them directionally. */
void vssg_node_disconnect(struct vssg_node *src, struct vssg_node *dst);

/** Removes references to `dst` from other nodes, using the `node->backrefs` array, disconnecting it from the world. */
void vssg_node_disconnect_all(struct vssg_node *dst);


/* ALLOCATOR **********************************************************************************************************/

/** Base node allocator. Individual allocator implementations may use this struct as a header and extend it with their
 * own state data. Concrete allocator implementations are in `alloc.h`. */
struct vssg_allocator {
    /** Allocates `n` nodes, writing pointers to them into `out_nodes`. */
    int (*alloc_n)(struct vssg_allocator *allocator, size_t n, struct vssg_node **out_nodes);
    /** Frees the specified node. */
    void (*free)(struct vssg_allocator *allocator, struct vssg_node *node);
    /** Makes the allocator clean up after itself */
    void (*destroy_self)(struct vssg_allocator *allocator);
};

/** Allocates `n` nodes and writes pointers to them into `out_nodes`. */
static inline int vssg_alloc_n(struct vssg_allocator *allocator, size_t n, struct vssg_node **out_nodes)
{
    if (allocator->alloc_n)
        return allocator->alloc_n(allocator, n, out_nodes);
    else
        return ENOMEM;
}

/** Frees the specified `node`, if the `allocator` has any free logic. */
static inline void vssg_alloc_free(struct vssg_allocator *allocator, struct vssg_node *node)
{
    if (allocator->free)
        allocator->free(allocator, node);
}

/** Cleans up the `allocator`, if it has any cleanup logic. */
static inline void vssg_alloc_destroy(struct vssg_allocator *allocator)
{
    if (allocator->destroy_self)
        allocator->destroy_self(allocator);
}


/* HASHTABLE **********************************************************************************************************/

#define VSSG_HASHTABLE_SIZE 2053    /**< Size of the hashtable. */
#define VSSG_HASHTABLE_COLSIZE 16   /**< Number of fields accounting for collisions in the hashtable. */

/** Hashtable of nodes, mapped by ID. */
struct vssg_hashtable {
    struct vssg_node *table[VSSG_HASHTABLE_SIZE][VSSG_HASHTABLE_COLSIZE];
    struct vssg_hashtable *next;   /**< Next table. For when there are more collisions than `VSSG_GRAPH_TABLE_COL`. */
};


/* GRAPH **************************************************************************************************************/

/** A graph. Contains a hashtable of nodes with IDs as keys. */
struct vssg_graph {
    struct vssg_hashtable hashtable;
    size_t graph_size;
    uint64_t next_id;
    struct vssg_allocator *allocator;
};

/** Initializes the specified `graph`. Sets it to use `allocator` to allocate its nodes. */
static inline void vssg_graph_init(struct vssg_graph *graph, struct vssg_allocator *allocator)
{
    *graph = (struct vssg_graph) {
        .allocator = allocator
    };
}

/** Frees up the resources used by `graph`. It does not free `graph` itself, as it may have been stack-allocated. */
void vssg_graph_destroy(struct vssg_graph *graph);

/** Creates `num_nodes` of nodes in the specified `graph`. The nodes will have their IDs generated, all other fields
 * will be initialized to 0. Pointers to all of the newly created nodes will be written into `out_nodes`.
 *
 * Returns standard status codes (0 on success). */
int vssg_graph_create_nodes(struct vssg_graph *graph, size_t num_nodes, struct vssg_node **out_nodes);

/** Finds the node with the specified ID in the `graph` and returns a pointer to its position in the hashtable.
 * May return `NULL` if no such node is present. */
struct vssg_node **vssg_graph_find(struct vssg_graph *graph, uint64_t node_id);

/** Removes the specified node from the graph. Returns `true` if such a node existed and was removed, otherwise
 * `false`. */
bool vssg_graph_remove_node(struct vssg_graph *graph, uint64_t node_id);

/** Gets a pointer to the node with the specified ID from the `graph`. May return `NULL` if no such node is present. */
static inline struct vssg_node *vssg_graph_get(struct vssg_graph *graph, uint64_t node_id)
{
    struct vssg_node **node = vssg_graph_find(graph, node_id);
    return (node) ? *node : NULL;
}

/** Gets the first node that can be found in the `graph`. May return `NULL` if the `graph` is empty. */
struct vssg_node *vssg_graph_get_any(struct vssg_graph *graph);

/** Connects two nodes in the `graph` so that the line goes from node with `src_id` to node with `dst_id`. */
static inline int vssg_graph_connect_nodes(struct vssg_graph *graph, uint64_t src_id, uint64_t dst_id)
{
    struct vssg_node *src = vssg_graph_get(graph, src_id);
    struct vssg_node *dst = vssg_graph_get(graph, dst_id);
    if (!src || !dst)
        return ENODATA;
    vssg_node_connect(src, dst, graph->allocator);
    return 0;
}


/* GRAPH ITERATOR *****************************************************************************************************/

/** Graph iterator structure. May be used in a `for` loop like so:
 *
 * ```
 * for (struct vssg_graph_it it = vssg_graph_it_start(graph); !vssg_graph_it_end(&it); vssg_graph_it_next(&it)) { ... }
 * ``` */
struct vssg_graph_it {
    struct vssg_hashtable *ht;
    size_t i;
    size_t col;
};

/** Returns an initialized iterator, pointing to the first valid node in the specified `graph`. */
struct vssg_graph_it vssg_graph_it_start(struct vssg_graph *graph);

/** Checks whether the iterator is at the end of the graph. When this returns `true`, no more nodes can be returned by
 * this iterator. */
static inline bool vssg_graph_it_end(const struct vssg_graph_it *it)
{
    return !it->ht;
}

/** Gets the node that this iterator is currently pointing to. May return `NULL` if the iterator is at the end. */
static inline struct vssg_node *vssg_graph_it_get(const struct vssg_graph_it *it)
{
    return it->ht->table[it->i][it->col];
}

/** Advances the iterator to the next node in the graph. */
void vssg_graph_it_next(struct vssg_graph_it *it);


/* NODE QUEUE *********************************************************************************************************/

#define VSSG_QUEUE_BASE_LENGTH 1024

/** Queue structure for breadth-first searches in a `struct vssg_graph`. It uses a circular buffer to store the queue
 * entries, so that it does not have to do memory moves during pop operations. */
struct vssg_queue {
    size_t start;
    size_t length;
    size_t capacity;
    struct vssg_node **buffer;
};

/** Initializes the specified queue so that it's ready to be used. */
void vssg_queue_init(struct vssg_queue *q);

/** Frees up the resources of the specified queue. Does not free `q` itself, as it may have been stack-allocated. */
void vssg_queue_destroy(struct vssg_queue *q);

/** Adds `node` to the specified queue. The queue may get resized during this operation if there is no space left. */
void vssg_queue_push(struct vssg_queue *q, struct vssg_node *node);

/** Gets a node from the back of the specified queue. */
struct vssg_node *vssg_queue_pop(struct vssg_queue *q);


/* NODE STACK *********************************************************************************************************/

#define VSSG_STACK_BASE_LENGTH 1024

/** Stack structure for depth-first searches in a `struct vssg_graph`. */
struct vssg_stack {
    size_t length;
    size_t capacity;
    struct vssg_node **buffer;
};

/** Initializes the specified stack so that it's ready to be used. */
void vssg_stack_init(struct vssg_stack *s);

/** Frees up the resources of the specified stack. Does not free `s` itself, as it may have been stack-allocated. */
void vssg_stack_destroy(struct vssg_stack *s);

/** Adds `node` to the specified stack. The stack may get resized during this operation if there is no space left. */
void vssg_stack_push(struct vssg_stack *s, struct vssg_node *node);

/** Gets a node from the top of the specified stack. */
struct vssg_node *vssg_stack_pop(struct vssg_stack *s);

#endif /* VSSG_GRAPH_H */
