/**@file Graph generator - creates instruction files. */

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "alloc.h"
#include "graph.h"
#include "grinst.h"
#include "util.h"

#define SIZES_OFFSET 2

#define VALUE_MIN -10
#define VALUE_MAX 10

#define BURST_MIN 1024
#define BURST_MAX 8192

#define CONN_MIN 2
#define CONN_MAX 10

static int delete_nodes(struct vssg_graph *g, ssize_t size, lcg_t *l, FILE *out)
{
    for (size_t i = 0; i < size; i++) {
        while (true) {
            uint64_t id = lcg_next_u(l) % g->next_id;
            bool deleted = vssg_graph_remove_node(g, id);
            if (!deleted)
                continue;
            int err = vssg_inst_write_delete(out, id);
            if (err)
                return err;
            break;
        };
    }
    return 0;
}

static int create_nodes(struct vssg_graph *g, ssize_t size, lcg_t *l, FILE *out)
{
    int err;
    uint64_t min_id = g->next_id;
    size_t count;

    /* Generate new nodes */
    for (count = 0; count < size;) {
        size_t burst = BURST_MIN + (lcg_next_u(l) % (BURST_MAX - BURST_MIN));
        if (size - count < burst) {
            burst = size - count;
        }
        struct vssg_node *nodes[burst];

        err = vssg_graph_create_nodes(g, burst, nodes);
        if (err)
            return err;

        err = vssg_inst_write_create(out, burst);
        if (err)
            return err;

        for (size_t i = 0; i < burst; i++) {
            nodes[i]->value = VALUE_MIN + (lcg_next_u(l) % (VALUE_MAX - VALUE_MIN));
            err = vssg_inst_write_set_value(out, nodes[i]->id, nodes[i]->value);
            if (err)
                return err;
        }

        count += burst;
    }

    uint64_t max_id = g->next_id;

    /* Generate connections between new nodes */
    for (size_t i = min_id; i < max_id; i++) {
        size_t connections = CONN_MIN + (lcg_next_u(l) % (CONN_MAX - CONN_MIN));
        for (size_t j = 0; j < connections; j++) {
            uint64_t src_id = i;

            while(true) {
                uint64_t dst_id = min_id + (lcg_next_u(l) % (max_id - min_id));
                err = vssg_graph_connect_nodes(g, src_id, dst_id);
                if (err == ENODATA) /* These IDs don't exist */
                    continue;
                else if (err)
                    return err;

                err = vssg_inst_write_connect(out, src_id, dst_id);
                if (err)
                    return err;
                break;
            }
        }
    }

    if (min_id == 0) {
        /* There are no old nodes yet, so we can't make connections to them. */
        return 0;
    }

    /* Generate connections from new nodes to old ones */
    size_t connections = CONN_MIN + (lcg_next_u(l) % (CONN_MAX - CONN_MIN));
    for (size_t j = 0; j < connections; j++) {
        while (true) {
            uint64_t src_id = min_id + (lcg_next_u(l) % (max_id - min_id));
            uint64_t dst_id = lcg_next_u(l) % min_id;
            err = vssg_graph_connect_nodes(g, src_id, dst_id);
            if (err == ENODATA) /* These IDs don't exist */
                continue;
            else if (err)
                return err;

            err = vssg_inst_write_connect(out, src_id, dst_id);
            if (err)
                return err;
            break;
        };
    }

    /* Generate connections from old nodes to new ones */
    connections = CONN_MIN + (lcg_next_u(l) % (CONN_MAX - CONN_MIN));
    for (size_t j = 0; j < connections; j++) {
        while (true) {
            uint64_t src_id = lcg_next_u(l) % min_id;
            uint64_t dst_id = min_id + (lcg_next_u(l) % (max_id - min_id));
            err = vssg_graph_connect_nodes(g, src_id, dst_id);
            if (err == ENODATA) /* These IDs don't exist */
                continue;
            else if (err)
                return err;

            err = vssg_inst_write_connect(out, src_id, dst_id);
            if (err)
                return err;
            break;
        };
    }

    return 0;
}

int main(int argc, char **argv)
{
    /* Prepare arguments */
    if (argc <= SIZES_OFFSET) {
        printf("Usage: %s <output_file> <size_1> [size_n ...]\n", argv[0]);
        return 1;
    }

    const size_t num_sizes = argc - SIZES_OFFSET;
    ssize_t sizes[num_sizes];
    ssize_t sum = 0;
    for (size_t i = 0; i < num_sizes; i++) {
        char *endptr = NULL;
        sizes[i] = strtoll(argv[SIZES_OFFSET + i], &endptr, 10);
        if (errno) {
            int errnum = errno;
            printf("Could not parse '%s': %s\n", argv[SIZES_OFFSET + i], strerror(errnum));
            return 1;
        }
        if (endptr && *endptr) {
            printf("Could not parse '%s'.\n", argv[SIZES_OFFSET + i]);
            return 1;
        }

        sum += sizes[i];
        if (sum < 0) {
            printf("Invalid sizes - cannot remove more nodes than there are in the graph\n");
            return 1;
        }
    }

    /* Open grinst file */
    FILE *out = fopen(argv[1], "w");
    if (!out) {
        perror("Could not open output file");
        return 1;
    }

    /* Generate graph */
    int retval = 0;
    struct vssg_graph g;
    vssg_graph_init(&g, vssg_linear_mmap_allocator_create());
    lcg_t l = lcg_seed_time();

    for (size_t i = 0; i < num_sizes; i++) {
        int err;
        if (sizes[i] > 0) {
            err = create_nodes(&g, sizes[i], &l, out);
        } else if (sizes[i] < 0) {
            err = delete_nodes(&g, -sizes[i], &l, out);
        } else {
            err = 0;
            fputs("0 encountered, skipping ...\n", stderr);
        }

        if (err) {
            fprintf(stderr, "Generation error: %s\n", strerror(err));
            retval = 1;
            goto exit_cleanup;
        }
    }

exit_cleanup:
    vssg_graph_destroy(&g);
    int out_end = fclose(out);
    if (out_end == EOF) {
        perror("Output file not closed properly");
        retval = 1;
    }

    return retval;
}
