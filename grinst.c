#include <endian.h>

#include "grinst.h"

#define INST_BUF_LENGTH 16384

int vssg_inst_interpret(struct vssg_graph *graph, struct vssg_inst *inst)
{
    switch (le32toh(inst->opcode)) {
        case VSSG_INST_NOP: {
            return 0;
        }

        case VSSG_INST_CREATE: {
            return vssg_graph_create_nodes(graph, (size_t) le64toh(inst->param.count), NULL);
        }

        case VSSG_INST_DELETE: {
            bool success = vssg_graph_remove_node(graph, le64toh(inst->param.node.id));
            return (success) ? 0 : ENODATA;
        }

        case VSSG_INST_SET_VALUE: {
            struct vssg_node *node = vssg_graph_get(graph, le64toh(inst->param.node.id));
            if (!node)
                return ENODATA;

            node->value = le64toh(inst->param.node.value);
            return 0;
        }

        case VSSG_INST_CONNECT: {
            return vssg_graph_connect_nodes(graph,
                    le64toh(inst->param.connection.src_id),
                    le64toh(inst->param.connection.dst_id));
        }

        default:
            return EINVAL;
    }
}

int vssg_inst_interpret_file(struct vssg_graph *graph, FILE *file)
{
    struct vssg_inst buf[INST_BUF_LENGTH];
    int err;

    while (!feof(file)) {
        size_t read = fread(buf, sizeof(struct vssg_inst), INST_BUF_LENGTH, file);
        err = ferror(file);
        if (err)
            return err;

        for (size_t i = 0; i < read; i++) {
            err = vssg_inst_interpret(graph, &buf[i]);
            if (err)
                return err;
        }
    }

    return 0;
}

static inline int write_inst(FILE* file, struct vssg_inst *inst)
{
    fwrite(inst, sizeof(*inst), 1, file);
    return ferror(file);
}

int vssg_inst_write_nop(FILE *file)
{
    struct vssg_inst inst = {
        .opcode = htole32(VSSG_INST_NOP)
    };
    return write_inst(file, &inst);
}

int vssg_inst_write_create(FILE *file, size_t count)
{
    struct vssg_inst inst = {
        .opcode = htole32(VSSG_INST_CREATE),
        .param = {
            .count = htole64((uint64_t) count)
        }
    };
    return write_inst(file, &inst);
}

int vssg_inst_write_delete(FILE *file, uint64_t id)
{
    struct vssg_inst inst = {
        .opcode = htole32(VSSG_INST_DELETE),
        .param = { .node = { .id = htole64(id) } }
    };
    return write_inst(file, &inst);
}

int vssg_inst_write_set_value(FILE *file, uint64_t id, int64_t value)
{
    struct vssg_inst inst = {
        .opcode = htole32(VSSG_INST_SET_VALUE),
        .param = {
            .node = {
                .id = htole64(id),
                .value = htole64(value)
            }
        }
    };
    return write_inst(file, &inst);
}

int vssg_inst_write_connect(FILE *file, uint64_t src_id, uint64_t dst_id)
{
    struct vssg_inst inst = {
        .opcode = htole32(VSSG_INST_CONNECT),
        .param = {
            .connection = {
                .src_id = htole64(src_id),
                .dst_id = htole64(dst_id)
            }
        }
    };
    return write_inst(file, &inst);
}
