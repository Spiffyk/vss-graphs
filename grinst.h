/**@file Graph building instructions */

#ifndef VSSG_GRINST_H
#define VSSG_GRINST_H

#include <stdint.h>
#include <stdio.h>

#include "graph.h"

/** Instruction opcodes. */
enum vssg_inst_opcode {
    /** No-op instruction */
    VSSG_INST_NOP       = 0,

    /** Create `n` nodes. Takes `param.count`. */
    VSSG_INST_CREATE    = 1,

    /** Delete node with the specified id. Takes `param.node.id`. */
    VSSG_INST_DELETE    = 2,

    /** Set value of the node with the specified id. Takes `param.node.id` and `param.node.value`. */
    VSSG_INST_SET_VALUE = 3,

    /** Connects nodes together. Takes `param.connection.src_id` and `param.connection.dst_id`. */
    VSSG_INST_CONNECT   = 4,
};

/** Instruction structure. All instructions are the same size. An instruction MUST always be 24 bytes long.
 * Fields unused by a specific instruction SHOULD be zero-initialized. */
struct vssg_inst {
    uint32_t opcode;    /**< Instruction opcode. Expected to be one of `enum vss_inst_opcode`. */
    uint32_t reserve1;  /**< Unused 32 bits. */
    union {
        uint64_t count;
        struct {
            uint64_t id;
            int64_t value;
        } node;
        struct {
            uint64_t src_id;
            uint64_t dst_id;
        } connection;
    } param;            /**< Instruction parameter. Contents vary according to the opcode. */
};

/** Interprets a single `inst` and saves the result into `graph`. The instruction data must be in Little-Endian,
 * as saved in an input file. */
int vssg_inst_interpret(struct vssg_graph *graph, struct vssg_inst *inst);

/** Interprets the specified `file` and saves the result into `graph`. The data in the file is expected to be saved
 * in Little-Endian. */
int vssg_inst_interpret_file(struct vssg_graph *graph, FILE *file);

/** Writes a `NOP` instruction into the specified `file`. */
int vssg_inst_write_nop(FILE *file);

/** Writes a `CREATE` instruction into the specified `file`. */
int vssg_inst_write_create(FILE *file, size_t count);

/** Writes a `DELETE` instruction into the specified `file`. */
int vssg_inst_write_delete(FILE *file, uint64_t id);

/** Writes a `SET_VALUE` instruction into the specified `file`. */
int vssg_inst_write_set_value(FILE *file, uint64_t id, int64_t value);

/** Writes a `CONNECT` instruction into the specified `file`. */
int vssg_inst_write_connect(FILE *file, uint64_t src_id, uint64_t dst_id);

#endif /* VSSG_GRINST_H */
