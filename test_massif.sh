#!/bin/sh
set -e

if [ $# -ne 2 ]; then
    echo "Usage: $0 <path_to_alloctest> <path_to_graph>"
    exit 1
fi

executable="$1"
graph="$2"

function run_alloctest() {
    local allocator="$1"

    echo "=== Testing $allocator ==="
    valgrind --tool=massif --pages-as-heap=yes --massif-out-file="massif/massif.out.$allocator" -- \
        "$executable" "$graph" "$allocator"
    echo
}


run_alloctest 'dumb-malloc'
run_alloctest 'cluster-malloc'
run_alloctest 'linear-mmap'
run_alloctest 'linear-mmap-freeable'
