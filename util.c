#include "util.h"

uint64_t clock_millis(clockid_t clock)
{
    struct timespec ts;
    clock_gettime(clock, &ts);
    uint64_t result = ts.tv_nsec / 1000000;
    result += ts.tv_sec * 1000;
    return result;
}
