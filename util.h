#ifndef VSSG_UTIL_H
#define VSSG_UTIL_H

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

/* TIME ***************************************************************************************************************/

/** Gets the current time of the specified clock in milliseconds. */
uint64_t clock_millis(clockid_t clock);


/* RANDOM NUMBER GENERATOR ********************************************************************************************/

#define LCG_A 6364136223846793005ULL
#define LCG_C 1L

/** The state of a linear congruential generator (LCG) for pseudo-random number generation. Implemented in this project
 * because in some libraries, the number of bits from rand() may actually be as low as 16, which would be insufficient
 * for the graph generator. Constants are taken from the MUSL library. */
typedef uint64_t lcg_t;

/** Creates a new LCG seeded with the specified value. */
static inline lcg_t lcg_seed(uint64_t seed)
{
    return seed;
}

/** Creates a new LCG seeded with the current system time. */
static inline lcg_t lcg_seed_time()
{
    return clock_millis(CLOCK_MONOTONIC);
}

/** Generates the next random number in the LCG. */
static inline void _lcg_next(lcg_t *gen)
{
    *gen = LCG_A * (*gen) + LCG_C;
}

/** Gets the next random unsigned int in the LCG. */
static inline uint32_t lcg_next_u(lcg_t *gen)
{
    _lcg_next(gen);
    return (uint32_t) (*gen >> 33);
}

/** Gets the next random signed int in the LCG. */
static inline int32_t lcg_next_i(lcg_t *gen)
{
    _lcg_next(gen);
    return (int32_t) (*gen >> 33);
}

/** Gets the next random float in the range 0.0-1.0 in the LCG. */
static inline float lcg_next_f(lcg_t *gen)
{
    _lcg_next(gen);
    return (float) ((double) lcg_next_u(gen) / (double) UINT32_MAX);
}


/* STRUCT OFFSETS *****************************************************************************************************/

/** Gets the pointer offset of the specified `field` from the specified structure `type`. May be used for some pointer
 * arithmetic magic. */
#define offsetof(type, field) (&((type *) NULL)->field)

/** Gets the pointer to a field in `cont` specified by `offset`. Uses the GNU extension that allows using pointer
 * arithmetics with `void *`. */
#define offsetattr(cont, offset) ((void *) (cont)) + ((size_t) (offset))


/* BITWISE MANIPULATIONS **********************************************************************************************/

static inline void set_bit(uint8_t *bitmap, size_t bit)
{
    bitmap[bit / 8] |= (1 << (bit % 8));
}

static inline void reset_bit(uint8_t *bitmap, size_t bit)
{
    bitmap[bit / 8] &= ~(1 << (bit % 8));
}


#endif /* VSSG_UTIL_H */
